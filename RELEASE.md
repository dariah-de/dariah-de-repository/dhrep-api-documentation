# Release of DARIAH-DE Repository Documentation

Please just commit and push to develop and then use the [DARIAH-DE Release Workflow](https://wiki.de.dariah.eu/pages/viewpage.action?spaceKey=DARIAH3&title=DARIAH-DE+Release+Management#DARIAHDEReleaseManagement-ReleasemitGitlab-CI).

No versions have to be changed, all versioning is done in gitlab CI/CD.
