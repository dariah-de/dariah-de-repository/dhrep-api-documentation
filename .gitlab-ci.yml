###
# PREPARATIONS
###

# Some variables
variables:
  BRANCH_NAME: $CI_COMMIT_REF_NAME
  PIPELINE_ID: $CI_PIPELINE_IID
  ADD_TO_DEB_FILE_PACKAGE_COUNT: 1027

# Include Java settings from Gitlab templates repo.
include:
  - project: 'dariah-de/gitlab-templates'
    ref: 'main'
    file: '/templates/.java.gitlab-ci.yml'

# Stages...
stages:
  - prepare
  - build
  - deploy

###
# JOBS
###

# Prepare (build) HTML documentation.
build-html-doc:
  only:
    - develop
    - main
  image: python:3.10
  stage: prepare
  script:
    - git config pull.rebase false
    - git checkout ${BRANCH_NAME}
    - git pull
    - git submodule init
    - git submodule update --remote --force
    - git submodule foreach git checkout ${BRANCH_NAME}
    - git submodule foreach git pull
    - cd docs
    - apt-get update && apt-get -y install python3-virtualenv
    - virtualenv venv
    - pwd . venv/bin/activate
    - pip install -r requirements.txt
    - snapshot="-SNAPSHOT"
    - if test "$BRANCH_NAME" = 'main'; then snapshot=""; fi
    - RELEASE_DATE="$(date +'%Y-%m-%d')${snapshot}"
    - RELEASE_VERSION="$(date +'%Y-%m')${snapshot}"
    - make clean html SPHINXOPTS="-D version=${RELEASE_VERSION} -D release=${RELEASE_DATE}"
  artifacts:
    paths:
      - docs/_build/html

# Build DEB files.
build-snapshot-deb:
  only:
    - develop
  image: ruby:3.0
  stage: build
  variables:
    name: 'dariah-de-doc'
    description: 'DARIAH-DE Repository Documentation'
    url: 'https://de.dariah.de'
    build_number: $PIPELINE_ID
    version: '2023-SNAPSHOT'
    deb_file_name: '${name}_${version}.deb'
  script:
    - gem install fpm
    # Iteration for DEB file iteration param only! Can not get this iteration to $deb_file_name var from bash scripting! Care about that in webapp template by renaming the deb file!
    - ITERATION=$(expr ${build_number} + $ADD_TO_DEB_FILE_PACKAGE_COUNT)
    - 'fpm -p $deb_file_name
            -t deb -a noarch -s dir
            --name ${name}
            --description "${description}"
            --maintainer "DARIAH-DE <info@de.dariah.eu>"
            --vendor "DARIAH-DE"
            --url ${url}
            --iteration $ITERATION
            --version ${version}
            -x ".git**"
            -x "**/.git**"
            -x "**/.hg**"
            -x "**/.svn**"
            -x ".buildinfo"
            -x "**/*.deb"
            --prefix /var/www/doc/services
            -C docs/_build/html .'
  artifacts:
    paths:
      - $deb_file_name

build-release-deb:
  only:
    - main
  image: ruby:3.0
  stage: build
  variables:
    name: 'dariah-de-doc'
    description: 'DARIAH-DE Repository API Documentation'
    url: 'https://de.dariah.eu'
    build_number: $PIPELINE_ID
    version: '2023-03'
    deb_file_name: '${name}_${version}.deb'
  script:
    - gem install fpm
    # Iteration for DEB file iteration param only! Can not get this iteration to $deb_file_name var from bash scripting! Care about that in webapp template by renaming the deb file!
    - ITERATION=$(expr ${build_number} + $ADD_TO_DEB_FILE_PACKAGE_COUNT)
    - 'fpm -p $deb_file_name
            -t deb -a noarch -s dir
            --name ${name}
            --description "${description}"
            --maintainer "DARIAH-DE <info@de.dariah.eu>"
            --vendor "DARIAH-DE"
            --url ${url}
            --iteration $ITERATION
            --version ${version}
            -x ".git**"
            -x "**/.git**"
            -x "**/.hg**"
            -x "**/.svn**"
            -x ".buildinfo"
            -x "**/*.deb"
            --prefix /var/www/doc/services
            -C docs/_build/html .'
  artifacts:
    paths:
      - $deb_file_name

# Deploy DEB files to APTLY repository.
deploy-snapshot-deb:
  stage: deploy
  only:
    - develop
  variables:
    PNAME: 'dariah-de-doc'
    PPATH: '.'
    PVERSION: '2023-SNAPSHOT'
    PKEY: 'SNAPSHOT'
    APTLY_TARGET: 'indy-snapshots'
    build_number: $PIPELINE_ID
    deb_file_name: '${PNAME}_${PVERSION}.deb'
  extends:
    - .deploy-deb-doc

deploy-release-deb:
  stage: deploy
  only:
    - main
  variables:
    PNAME: 'dariah-de-doc'
    PPATH: '.'
    PVERSION: '2023-03'
    PKEY: ''
    APTLY_TARGET: 'indy-releases'
    build_number: $PIPELINE_ID
    deb_file_name: '${PNAME}_${PVERSION}.deb'
  extends:
    - .deploy-deb-doc
