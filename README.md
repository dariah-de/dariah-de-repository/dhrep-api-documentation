# DARIAH-DE Repository and API Documentation

This repository gathers all documentation of DH-rep services and deploys them as the DARIAH-DE Repository Documentation to https://repository.de.dariah.eu, https://dhrepworkshop.de.dariah.eu/doc, and https://trep.de.dariah.eu/doc.

Please initially do a:

    $ git submodule init

and then each time to update modules:

    $ git submodule update --force --remote

For every push into the develop branch a SNAPSHOT build is being performed (and deployed to trep.de.dariah.eu), the main branch is build as a RELEASE build (and deployed to repository.de.dariah.eu and dhrepworkshop.de.dariah.eu). For more information please see RELEASE.md.

If you want to add a new submodule, please use:

    $ git submodule add [git repo of module] [folder of submodule]


## Release of DARIAH-DE Repository Documentation

Please just commit and push to develop and then use the [DARIAH-DE Release Workflow](https://wiki.de.dariah.eu/pages/viewpage.action?spaceKey=DARIAH3&title=DARIAH-DE+Release+Management#DARIAHDEReleaseManagement-ReleasemitGitlab-CI).

No versions have to be changed, all versioning is done in gitlab CI/CD.
