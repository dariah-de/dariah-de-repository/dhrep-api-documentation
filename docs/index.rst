.. DARIAH-DE Repository documentation master file, created by
   sphinx-quickstart on Wed May 20 16:07:38 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


The DARIAH-DE Repository Documentation
======================================

DARIAH-DE operates a repository as a digital long-term archive for humanities and cultural-scientific research data. The `DARIAH-DE Repository <https://repository.de.dariah.eu>`__ is a central component of the `DARIAH-DE Research Data Federation Architecture <https://de.dariah.eu/en/web/guest/weiterfuhrende-informationen>`__, which aggregates various services and applications and can be used comfortably by DARIAH users.
The DARIAH-DE Repository allows you to save your research data in a sustainable and secure way, to provide it with metadata and to publish it. Your collection as well as each individual file is available in the DARIAH-DE Repository in the long term and gets a unique and permanently valid `Digital Object Identifier <https://www.doi.org>`__ (DOI) with which your data can be permanently referenced and cited. Your collections are registered within the `Repository Collection Registry <https://repository.de.dariah.eu/colreg-ui>`__ and the `Repository Search <https://repository.de.dariah.eu>`__. They can also be found in the `DARIAH-DE Generic Search <https://search.de.dariah.eu>`__ and the `DataCite Search <https://search.datacite.org/>`__.

On this site you can find information on organizational infrastructure, data policies, and digital object management of the DARIAH-DE Repository, user guides and public APIs are documented as well. They provide information on access, import, dissemination, and resolving. You can find more information about the DARIAH-DE Repository at the `DARIAH-DE Portal <https://de.dariah.eu/repository>`__. Easy import and publish features you can find with the `DARIAH-DE Publikator <https://de.dariah.eu/publikator>`__.


.. toctree::
  :maxdepth: 2
  :caption: DARIAH-DE Repository

  organizational-infrastructure
  data-policies
  digital-object-management


.. toctree::
  :maxdepth: 2
  :caption: User Guides

  submodules/publikator/docs/index
  submodules/publikator/docs/index-de


.. toctree::
  :maxdepth: 2
  :caption: Service APIs

  submodules/dhcrud/dhcrud-webapp-public/docs/index
  submodules/dhdigilib/docs_dhrep/index
  submodules/dhpublish/kolibri-dhpublish-service/docs/index
  submodules/dhoaipmh/docs_dhrep/index
  submodules/dhiiifmd/docs_dhrep/index


.. toctree::
  :maxdepth: 2
  :caption: Resolving

  resolving


Schema Use Notice
=================

Portions of this software may use (XML ¦ RDF) schemas Copyright © 2011 `DCMI <http://dublincore.org/>`_, the Dublin Core Metadata Initiative. These are licensed under the `Creative Commons 4.0 Attribution <https://creativecommons.org/licenses/by/4.0/>`_ license.
