Resolving and Persistent Identifiers
====================================

Each object published in the DARIAH-DE Repository has got two persistant identifiers, which are created during import process: a `DataCite DOI <https://www.datacite.org/>`_ for citation, and an `EPIC Handle PID <https://www.pidconsortium.net/>`_ for administrative use.

DataCite DOI and EPIC Handle prefixes are institution specific, and the suffixes for DARIAH-DE Repository DOIs and Handles are just same, such as:

- **10.20375/0000-000B-C8EF-7** (DataCite DOI)
- **21.11113/0000-000B-C8EF-7** (EPIC Handle)


Both DataCite DOIs and EPIC Handles can be resolved by any DOI or Handle resolver. With each EPIC Handle some administrative metadata is stored, that is providing access to all the object's data and metadata stored in the repository, and an URL that points directly to the object in the DARIAH-DE PublicStorage.

Each object is stored as a `Bagit <https://tools.ietf.org/html/draft-kunze-bagit>`_ bag in the DARIAH-DE PublicStorage, where it can be accessed publicly. A BagIt profile for the DARIAH-DE Repository Bags is provided in `Version 0.2 <https://repository.de.dariah.eu/schemas/bagit/profiles/dhrep_0.2.json>`_.

Access to the repository's objects is provided using HTTP content negotiation with the basic DOI or Handle. You can get:

1. The complete bag (as ZIP) setting HTTP's Accept-Header to **application/zip**.
2. The HTML landing page if requesting **text/html**.
3. The data object itself otherwise.

You can access the repository's content using the DOI, the Handle, and directly via the DH-crud URL.

If the object's data is a ZIP or HTML file, please use the access methods provided below.

Access via DataCite DOI
-----------------------

Using the DOI identifiers is recommended for citation and object access.

- `https://dx.doi.org/10.20375/0000-000B-C8EF-7 <https://dx.doi.org/10.20375/0000-000B-C8EF-7>`_ (via content negotiation)
- `https://hdl.handle.net/21.11113/0000-000B-C8EF-7 <https://hdl.handle.net/21.11113/0000-000B-C8EF-7>`_ (via content negotiation)

DataCite Metadata
^^^^^^^^^^^^^^^^^

You can get the DataCite DOI Metadata via `DOI Content Negotiation <https://citation.crosscite.org/docs.html>`_:

- `https://data.datacite.org/application/vnd.datacite.datacite+xml/10.20375/0000-000B-C8EF-7 <https://data.datacite.org/application/vnd.datacite.datacite+xml/10.20375/0000-000B-C8EF-7>`_ (DataCite XML)
- `https://data.datacite.org/application/rdf+xml/10.20375/0000-000B-C8EF-7 <https://data.datacite.org/application/rdf+xml/10.20375/0000-000B-C8EF-7>`_ (DataCite RDF/XML)

DataCite Citation
^^^^^^^^^^^^^^^^^

DataCite also provides citation links for your DOI documents via `DOI Content Negotiation <https://citation.crosscite.org/docs.html>`_:

-  `https://data.datacite.org/application/vnd.citationstyles.csl+json/10.20375/0000-000B-C8EF-7 <https://data.datacite.org/application/vnd.citationstyles.csl+json/10.20375/0000-000B-C8EF-7>`_ (Citeproc JSON)
- `https://data.datacite.org/application/vnd.schemaorg.ld+json/10.20375/0000-000B-C8EF-7 <https://data.datacite.org/application/vnd.schemaorg.ld+json/10.20375/0000-000B-C8EF-7>`_ (JSON-LD)
- `https://data.datacite.org/text/x-bibliography/10.20375/0000-000B-C8EF-7 <https://data.datacite.org/text/x-bibliography/10.20375/0000-000B-C8EF-7>`_ (formatted text citation)
- `https://data.datacite.org/application/x-research-info-systems/10.20375/0000-000B-C8EF-7 <https://data.datacite.org/application/x-research-info-systems/10.20375/0000-000B-C8EF-7>`_ (RIS)
- `https://data.datacite.org/application/x-bibtex/10.20375/0000-000B-C8EF-7 <https://data.datacite.org/application/x-bibtex/10.20375/0000-000B-C8EF-7>`_ (BibTeX)
- `https://data.datacite.org/application/vnd.crossref.unixref+xml/10.20375/0000-000B-C8EF-7 <https://data.datacite.org/application/vnd.crossref.unixref+xml/10.20375/0000-000B-C8EF-7>`_ (CrossRef Unixref XML)

DataCite Search
^^^^^^^^^^^^^^^

The `DataCite Search <https://search.datacite.org/>`__ also provides access to the data of the DARIAH-DE Repository.


Access via EPIC Handle (Templates)
----------------------------------

Data
^^^^

- `https://hdl.handle.net/21.11113/0000-000B-C8EF-7@data <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@data>`_

BagIt Bag (ZIP)
^^^^^^^^^^^^^^^

- `https://hdl.handle.net/21.11113/0000-000B-C8EF-7@bag <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@bag>`_

Descriptive Metadata
^^^^^^^^^^^^^^^^^^^^

You can request the descriptive RDF metadata of each object in various formats:

- `https://hdl.handle.net/21.11113/0000-000B-C8EF-7@metadata <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@metadata>`_ (same as `https://hdl.handle.net/21.11113/0000-000B-C8EF-7@metadata/ttl <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@metadata/ttl>`_)
- `https://hdl.handle.net/21.11113/0000-000B-C8EF-7@metadata/xml <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@metadata/xml>`_
- `https://hdl.handle.net/21.11113/0000-000B-C8EF-7@metadata/json <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@metadata/json>`_
- `https://hdl.handle.net/21.11113/0000-000B-C8EF-7@metadata/jsonld <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@metadata/jsonld>`_
- `https://hdl.handle.net/21.11113/0000-000B-C8EF-7@metadata/ntriples <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@metadata/ntriples>`_

Administrative Metadata
^^^^^^^^^^^^^^^^^^^^^^^

Also administrative RDF metadata is provided in various formats:

- `https://hdl.handle.net/21.11113/0000-000B-C8EF-7@adm <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@adm>`_ (same as `https://hdl.handle.net/21.11113/0000-000B-C8EF-7@adm/ttl <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@adm/ttl>`_)
- `https://hdl.handle.net/21.11113/0000-000B-C8EF-7@adm/xml <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@adm/xml>`_
- `https://hdl.handle.net/21.11113/0000-000B-C8EF-7@adm/json <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@adm/json>`_
- `https://hdl.handle.net/21.11113/0000-000B-C8EF-7@adm/jsonld <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@adm/jsonld>`_
- `https://hdl.handle.net/21.11113/0000-000B-C8EF-7@adm/ntriples <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@adm/ntriples>`_

Technical Metadata
^^^^^^^^^^^^^^^^^^

Technical metadata is provided in FITS XML (`File Information Toolset <https://projects.iq.harvard.edu/fits>`_) only, see `FITS XML schema <http://hul.harvard.edu/ois/xml/xsd/fits/fits_output.xsd>`_:

- `https://hdl.handle.net/21.11113/0000-000B-C8EF-7@tech <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@tech>`_

Provenance Metadata
^^^^^^^^^^^^^^^^^^^

 The use of provenance metadata is not yet implemented.

- `https://hdl.handle.net/21.11113/0000-000B-C8EF-7@prov <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@prov>`_

Landing Page
^^^^^^^^^^^^

The landing page provides basic information about the object, and leads to easy data download and basic metadata.

- `https://hdl.handle.net/21.11113/0000-000B-C8EF-7@landing <https://hdl.handle.net/21.11113/0000-000B-C8EF-7@landing>`_


Access via DH-crud Directly
---------------------------

All DOI and Handle URLs are directly resolved to the DARIAH-DE CRUD service, that is unpacking the BagIt ZIPs from PublicStorage and delivering all the information, such as:

- `https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/metadata <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/metadata>`_
- `https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/adm <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/adm>`_
- `https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/tech <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/tech>`_
- `https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/prov <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/prov>`_ (not yet implemented)
- `https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/data <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/data>`_
- `https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/bag <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/bag>`_
- `https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/landing <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/landing>`_

Direct CRUD access is available using the EPIC Handle only at the moment.
