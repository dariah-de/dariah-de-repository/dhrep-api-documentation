Organisational Infrastructure
=============================

Mission and Long-Term Preservation
----------------------------------

The mission of the DARIAH-DE Repository is to serve nationally and internationally research, teaching and learning by providing long term preservation, continued access, reuse, openly sharing and dissemination of digital research data according to ethical and scientific standards of the research community. The publicly `stated mission <https://de.dariah.eu/en/mission-statement>`__ is approved by the `DARIAH-DE Coordination Office <https://de.dariah.eu/en/kontakt>`__.

The repository sees its mission in line with the `Open Access strategy of the University of Göttingen <https://www.uni-goettingen.de/en/221506.html>`__ and its `research data policy <http://www.uni-goettingen.de/en/488918.html>`__. It provides all necessary resources to promote and support making the research results of its researchers as widely accessible and usable as possible. This commitment to open access is reflected in the organisational and technical infrastructure as well as in its archiving procedures of the repository to allow the use of publications and data without any access restriction in order “to support research and innovation in science [...] and society in a direct and lasting way”. In terms of :doc:`Digital Object Management <digital-object-management>`, publication and preservation workflows are based on the Open Archiving Information System, see :doc:`Digital Object Management <digital-object-management>`.

The commitment is strongly supported by the two relevant institutions ensuring also the long-term sustainability of the repository and its data: The `Göttingen State and University Library <https://www.sub.uni-goettingen.de/en/about-us/portrait/>`__ (SUB) and the `Gesellschaft für wissenschaftliche Datenverarbeitung Göttingen mbH <https://www.gwdg.de/about-us>`__ (GWDG).

Both institutions share a commitment to the sustainability of services and to `FAIR principles <https://www.go-fair.org/fair-principles/>`__ in research and its infrastructures. For the SUB research data management is an important aspect of the `strategic aims of Göttingen State and University Library <https://www.sub.uni-goettingen.de/en/about-us/portrait/strategy/#c13124>`__. Not only for research data, but for all digital resources, Göttingen State and University Library follows a `policy <https://www.sub.uni-goettingen.de/en/about-us/portrait/goettingen-state-and-university-library-digital-policies-guiding-principles/>`__, which contains guiding principles in order to ensure the quality for access, metadata and IT architecture.

In the context of open access, the Göttingen State and University Library also participates in national and international projects, such as the `Confederation of Open Access Repositories <https://www.coar-repositories.org/>`__ (COAR) and `OpenAIRE <http://www.openaire.eu/>`__. In this perspective the DARIAH-DE Repository is also in line with open access requirements of important funders of the German research system as the German Research Foundation (DFG) (see https://www.dfg.de/formulare/2_00/v/dfg_2_00_de_v1215.pdf, p. 44, section 12.2.1) and the European Union. Mandates of the European Commission and the European Research Council require as stated e.g. in the European Open Access Pilot on Open Data all funded projects to publish their results in Open Access (see the `Horizon 2020 Online Manual <https://ec.europa.eu/research/participants/docs/h2020-funding-guide/cross-cutting-issues/open-access-data-management/open-access_en.htm>`__). The Research Department at Göttingen University offers detailed information about the `European Union Open Access Pilot <https://www.uni-goettingen.de/en/487290.html>`__ also on its web pages.

The DARIAH-DE Repository is a discipline specific repository and commits itself to ensure availability and long-term preservation of data in the Arts, Humanties and Cultural Studies. Because of its designated community and its history the DARIAH-DE Repository defines furthermore itself as an searchable and citable long-term archive and offers additional services. Further access is provided by the DARIAH-DE Repository APIs allowing permanent and referencable storage of data in different formats.


The DARIAH-DE Repository and its Designated Community
-----------------------------------------------------

The Designated Community consists of researchers of a wide array of subjects within the field of the Arts, Humanities, and Cultural Studies. Their interests and research objectives vary and, therefore, can be met with services and tools that cover those various demands. The DARIAH-DE Repository meets these needs. Although most of the users are from Germany, the DARIAH-DE Repository itself is open to users from other European countries and via EduGain worldwide as well. The DARIAH-DE Repository is strongly connected to the DARIAH-EU tools and services and currently fostered to become an European service connected to the EOSC Marketplace. The repository offers the opportunity to store and publish research data; both individual scholars of all qualification levels and research projects are invited to work with the repository. DARIAH-DE addresses specifically researchers in the Digital Humanities, respectively researchers in the Arts, Humanities, and Cultural Sciences who are using digital sources and tools for their research. Since DARIAH-DE I started in 2011, it established a broad range of community activities. By now, DARIAH-DE is an integral part of the scientific community with several vital sub-communities. The DARIAH-DE repository is placed within this community and interwoven with all activities in the network.

The researchers in focus of the DARIAH-DE Repository are predominantly working in temporary research projects at universities and other research institutions in Germany. Often, the researchers are working in small teams or on their own. In comparison to e.g., Life Sciences, the amount of data they produce are as a rule small but in respect to research data format diverse. A common trait of the work situation of the researchers is little support from their own institution concerning the research data management. The DARIAH-DE Repository offers a unique solution, as it enables the researchers to upload their research data by themselves and publish them without having to take many different hurdles.

The curation of the DARIAH-DE Repository involves a brief checking of basic metadata organized along the categories and premises of the Dublin Core Simple metadata schema; it comprises 15 elements, 3 of them are mandatory (title, author, license regulations), while the other 12 are optional. Otherwise, the research data are distributed as deposited. The main idea of the repository is that DARIAH-DE provides the tools as well as user counseling because they have the expert knowledge to describe content and veracity of the research data. Users may use the tools provided by the `DARIAH-DE Research Data Federation Infrastructure <https://de.dariah.eu/en/web/guest/weiterfuhrende-informationen>`__, for example in order to map their research data or to improve its findability.

The DARIAH-DE Repository has a **low threshold for its users**, concerning both the technical resources and the prior knowledge necessary for describing their research data appropriately. Each step of the process is done online via the `DARIAH-DE Publikator <https://repository.de.dariah.eu/publikator>`__ in a user friendly GUI. Furthermore, each step of the process is elaborately and precisely documented (:doc:`The DARIAH-DE Repository Documentation <index>`). In case of technical problems or further questions, the DARIAH-DE Helpdesk connects users with experts of DARIAH-DE within less than 48 hours. Articles from the users' point of view (see https://dhd-blog.org/?p=8798), FAQs (https://wiki.de.dariah.eu/display/publicde/FAQs+zum+Publikator), a user guide(:doc:`User Guide <submodules/publikator/docs/index>`), tutorials (see https://youtube.com/watch?v=4FFH5fJRLdU&t=304s), and workshops of the DARIAH-DE partners complete the support.


Organisational Infrastructure and Long-Term Sustainability
----------------------------------------------------------

In terms of long term operation, stability and sustainability the DARIAH-DE Repository is integrated in a complex organisational infrastructure as illustrated by fig. 1.

.. figure:: ./pics/2021-11-30-Organisational-Infrastructure-DARIAHRep.png
    :align: center
    :alt: Fig. 1: Organisational Infrastructure and Long-Term-Operation of the DARIAH-DE Repository
    :figclass: align-center

    Fig. 1: Organisational Infrastructure and Long-Term-Operation of the DARIAH-DE Repository


The DARIAH-DE Repository is operated by the `Humanities Data Centre <https://humanities-data-centre.de/>`__ (HDC) to ensure its long-term sustainability. The HDC has been founded explicitly for this purpose by the `Göttingen State and University Library <https://www.sub.uni-goettingen.de/en/about-us/portrait/>`__  (SUB) and the `Gesellschaft für wissenschaftliche Datenverarbeitung Göttingen mbH <https://www.gwdg.de/about-us>`__ (GWDG), which operates as computing centre and IT-competence centre for the University of Göttingen and the Max Planck Society. Both institutions guarantee the long-term stability and sustainability of the repository in addition and if necessary independently of public project funding -- as publicly declared in their `founding manifesto <http://humanities-data-centre.de/wp-content/uploads/2016/08/HDC_Erkl%C3%A4rung-Aufbau-Forschungsdatenzentrum_2016-07-27_gez.pdf>`__.

The DARIAH-DE Repository is since 2016 part of the DARIAH-DE Research Infrastructure which offers a variety of services and tools -- the DARIAH-DE Repository is one of them. DARIAH-DE and the member institutions are part of the `association for Research Infrastructures in the Humanities and Cultural Studies <http://www.textgrid-verein.de/>`__ (former TextGrid e.V.) to ensure the sustainability regardless of project funding. Additionally, the service and its operating institutions are also partner institutions of the long-term funding project and consortium `Text+ <https://www.text-plus.org/>`__ which is part of the `national research data infrastructure <https://www.nfdi.de/>`__ (NFDI).

The SUB and GWDG as founders of the HDC are also important consortium members of DARIAH-DE and Text+. While the SUB is responsible for the administrative coordination, the GWDG takes the responsibility for the technical coordination.

The specific governance structure of DARIAH-DE as an ERIC (`European Research Infrastructure Consortium <https://ec.europa.eu/info/research-and-innovation/strategy/strategy-2020-2024/our-digital-future/european-research-infrastructures/eric_en>`__) and of the association for Research Infrastructures in the Humanities and Cultural Studies and of Text+ ensures that disciplinary and technical innovations are taken into account and implemented to an appropriate extent. DARIAH-DE committed itself via the steering committee which is the joint central decision-making body to provide the DARIAH-DE Repository and its joint services as a yearly assessed `inkind contribution for the DARIAH ERIC <https://priddy.github.io/DARIAH-RA/in-kind/>`__. As written in the statutes of the association for Research Infrastructures in the Humanities and Cultural Studies the service portfolio of DARIAH-DE will be monitored in the coordination board of the association. In Text+ the DARIAH-DE Repository is part of the consortium infrastructure and the NFDI. As such it will be regulary reviewed and assessed by the `Operations Coordination Committee <https://www.text-plus.org/en/about-us/coordination-committees-2/>`__ and during the funding application process. Text+ provides additional resources in means of financial and human resources for proposing and developing specific extensions.

Strategic developments and potential disciplinary innovations concerning the repositories are implemented in two ways:

* Minor or regular adjustments are provided by the employees of the DARIAH-DE Coordination Office responsible for the DARIAH-DE Repository and the related virtual research environment.
* Project specific extensions are proposed and developed within the framework of projects, for example Text+.


Responsible Institutions: Rules and Obligations
-----------------------------------------------

As illustrated above for the organisational infrastructure of the DARIAH-DE Repository, SUB and GWDG as founders of Humanities Data Centre take over a variety of tasks within the framework of the research infrastructure DARIAH-DE which persits beyond the operating cooperation (service provider) and the associated project CLARIAH-DE. All tasks fulfilled by SUB and GWDG as running members of the HDC and consortium members of DARIAH-DE are in-house and listed in the following overview.


DARIAH-DE Services and Tasks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Göttingen State and University Library (SUB), administrative coordinator of the operating cooperation:
    * Publikator Application Management
    * DARIAH-DE Repository Service Management
    * Consulting for use of the repository
    * Consulting for ingesting large amounts of data
    * Dissemination
    * Maintenance
    * Updates
    * User request
    * Documentation
    * DARIAH-DE Repository workshops and training
* Gesellschaft für wissenschaftliche Datenverarbeitung Göttingen mbH (GWDG), technical coordination of the operating cooperation:
    * DARIAH Authentication and Authorization Infrastructure (DARIAH AAI)
    * Persistent Identifiers
    * Virtual Machines
    * Storage and backup
    * Monitoring


Text+ Services and Tasks
^^^^^^^^^^^^^^^^^^^^^^^^

* Project Partners (SUB, GWDG), see `applicant institutions <https://www.text-plus.org/en/about-us/applicant-institutions/>`__ and `participating institutions <https://www.text-plus.org/en/about-us/participating-institutions/>`__
    * Evaluation of new features for DARIAH-DE Repository
    * Implementation of new features for DARIAH-DE Repository
    * Evaluation of possibilities for format- and collection development


Expertise of the SUB and GWDG
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Software engineers
* Information scientists
* Metadata experts
* Experts for digital editions
* Digital Humanists
* Text and data mining experts
